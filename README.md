Issue Tracker
=============
<http://issues.marvelution.com/browse/SKINS>

Continuous Builder
==================
<http://builds.marvelution.com/browse/SKINS>

License
=======
[The Apache Software License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0.txt)
